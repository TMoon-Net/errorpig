﻿/// <summary>
/// 备注：tmoon
/// </summary>
public class AppConst 
{
    /// <summary>
    /// 调试模式-用于内部测试
    /// </summary>
    public const bool DebugMode = false;
    /// <summary>
    /// 更新模式-默认关闭 
    /// </summary>
    public const bool UpdateMode = true;
    /// <summary>
    /// Lua代码AssetBundle模式
    /// </summary>
    public const bool LuaBundleMode = true;                    

    /// <summary>
    /// 应用程序名称
    /// </summary>
    public const string AppName = "errorpig";
    /// <summary>
    /// 临时目录
    /// </summary>
    public const string LuaTempDir = "TempLua\\";
    /// <summary>
    /// 素材扩展名
    /// </summary>
    public const string ExtName = ".unity3d";
    /// <summary>
    /// 素材目录 
    /// </summary>
    public const string AssetDir = "StreamingAssets";  
    /// <summary>
    /// 更新地址
    /// </summary>
    public const string WebUrl = "http://192.168.137.1/";
}
