﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using LuaInterface;
using UObject = UnityEngine.Object;

public class AssetBundleInfo
{
    public AssetBundle m_AssetBundle;
    public int m_ReferencedCount;

    public AssetBundleInfo(AssetBundle assetBundle)
    {
        m_AssetBundle = assetBundle;
        m_ReferencedCount = 0;
    }
}

public class ResourceManager : MonoBehaviour
{
    public static ResourceManager Instance;

    private string m_BaseDownloadingURL = string.Empty;
    private string[] m_AllManifest = null;
    private AssetBundleManifest m_AssetBundleManifest = null;
    private Dictionary<string, string[]> m_Dependencies = new Dictionary<string, string[]>();
    private Dictionary<string, AssetBundleInfo> m_LoadedAssetBundles = new Dictionary<string, AssetBundleInfo>();
    private Dictionary<string, List<LoadAssetRequest>> m_LoadRequests = new Dictionary<string, List<LoadAssetRequest>>();

    private void Awake()
    {
        Instance = this;

        DontDestroyOnLoad(this.gameObject);
    }

    // Load AssetBundleManifest.
    public void Initialize(string manifestName, Action initOK)
    {
        // 获得对应平台数据包的存储路径
        m_BaseDownloadingURL = Util.GetRelativePath();
        // 加载AssetBundleManifest主资源
        LoadAsset<AssetBundleManifest>(manifestName, new string[] { "AssetBundleManifest" }, delegate (UObject[] objs)
        {
            if (objs.Length > 0)
            {
                m_AssetBundleManifest = objs[0] as AssetBundleManifest;
                m_AllManifest = m_AssetBundleManifest.GetAllAssetBundles();
            }
            initOK?.Invoke();
        });
    }

    public void LoadPrefab(string abName, string assetName, Action<UObject[]> func)
    {
        LoadAsset<GameObject>(abName, new string[] { assetName }, func);
    }

    public void LoadPrefab(string abName, string[] assetNames, Action<UObject[]> func)
    {
        LoadAsset<GameObject>(abName, assetNames, func);
    }

    public void LoadPrefab(string abName, string[] assetNames, LuaFunction func)
    {
        LoadAsset<GameObject>(abName, assetNames, null, func);
    }

    /// <summary>
    /// 此函数交给外部卸载专用，自己调整是否需要彻底清除AB
    /// </summary>
    /// <param name="abName"></param>
    /// <param name="isThorough"></param>
    public void UnloadAssetBundle(string abName, bool isThorough = false)
    {
        abName = GetRealAssetPath(abName);
        Debug.Log(m_LoadedAssetBundles.Count + " assetbundle(s) in memory before unloading " + abName);
        UnloadAssetBundleInternal(abName, isThorough);
        UnloadDependencies(abName, isThorough);
        Debug.Log(m_LoadedAssetBundles.Count + " assetbundle(s) in memory after unloading " + abName);
    }

    private class LoadAssetRequest
    {
        public Type assetType;
        public string[] assetNames;
        public LuaFunction luaFunc;
        public Action<UObject[]> sharpFunc;
    }

    /// <summary>
    /// 载入素材
    /// </summary>
    private void LoadAsset<T>(string abName, string[] assetNames, Action<UObject[]> action = null, LuaFunction func = null) where T : UObject
    {
        // 获得真实存在的ab包
        abName = GetRealAssetPath(abName);

        // 实例化加载请求
        LoadAssetRequest request = new LoadAssetRequest();
        request.assetType = typeof(T);
        request.assetNames = assetNames;
        request.luaFunc = func;
        request.sharpFunc = action;

        // 查询该ab包加载请求列表是否已存在
        // 存在的情况是前面已加载了该ab包  但还没完成使命的时候又加载这个ab包的内容
        List<LoadAssetRequest> requests = null;
        if (!m_LoadRequests.TryGetValue(abName, out requests))
        {
            requests = new List<LoadAssetRequest>();
            requests.Add(request);
            m_LoadRequests.Add(abName, requests);
            StartCoroutine(OnLoadAsset<T>(abName));
        }
        else
        {
            requests.Add(request);
        }
    }

    private IEnumerator OnLoadAsset<T>(string abName) where T : UObject
    {
        AssetBundleInfo bundleInfo = GetLoadedAssetBundle(abName);
        if (bundleInfo == null)
        {
            yield return StartCoroutine(OnLoadAssetBundle(abName, typeof(T)));

            bundleInfo = GetLoadedAssetBundle(abName);
            if (bundleInfo == null)
            {
                m_LoadRequests.Remove(abName);
                Debug.LogError("OnLoadAsset--->>>" + abName);
                yield break;
            }
        }
        List<LoadAssetRequest> list = null;
        if (!m_LoadRequests.TryGetValue(abName, out list))
        {
            m_LoadRequests.Remove(abName);
            yield break;
        }
        for (int i = 0; i < list.Count; i++)
        {
            string[] assetNames = list[i].assetNames;
            List<UObject> result = new List<UObject>();

            AssetBundle ab = bundleInfo.m_AssetBundle;
            for (int j = 0; j < assetNames.Length; j++)
            {
                string assetPath = assetNames[j];

                //异步加载
                AssetBundleRequest request = ab.LoadAssetAsync(assetPath, list[i].assetType);
                yield return request;
                result.Add(request.asset);

                T assetObj = ab.LoadAsset<T>(assetPath);
                result.Add(assetObj);

                // 同步加载
                //result.Add(ab.LoadAsset(assetPath, list[i].assetType));
            }
            if (list[i].sharpFunc != null)
            {
                list[i].sharpFunc(result.ToArray());
                list[i].sharpFunc = null;
            }
            if (list[i].luaFunc != null)
            {
                list[i].luaFunc.Call((object)result.ToArray());
                list[i].luaFunc.Dispose();
                list[i].luaFunc = null;
            }
            bundleInfo.m_ReferencedCount++;
        }
        m_LoadRequests.Remove(abName);
    }

    private IEnumerator OnLoadAssetBundle(string abName, Type type)
    {
        string url = m_BaseDownloadingURL + abName;

        WWW download = null;
        if (type == typeof(AssetBundleManifest))
            download = new WWW(url);
        else
        {
            string[] dependencies = m_AssetBundleManifest.GetAllDependencies(abName);
            if (dependencies.Length > 0)
            {
                m_Dependencies.Add(abName, dependencies);
                for (int i = 0; i < dependencies.Length; i++)
                {
                    string depName = dependencies[i];
                    AssetBundleInfo bundleInfo = null;
                    if (m_LoadedAssetBundles.TryGetValue(depName, out bundleInfo))
                    {
                        bundleInfo.m_ReferencedCount++;
                    }
                    else if (!m_LoadRequests.ContainsKey(depName))
                    {
                        yield return StartCoroutine(OnLoadAssetBundle(depName, type));
                    }
                }
            }
            download = WWW.LoadFromCacheOrDownload(url, m_AssetBundleManifest.GetAssetBundleHash(abName), 0);
        }
        yield return download;

        AssetBundle assetObj = download.assetBundle;
        if (assetObj != null)
        {
            m_LoadedAssetBundles.Add(abName, new AssetBundleInfo(assetObj));
        }
    }

    private AssetBundleInfo GetLoadedAssetBundle(string abName)
    {
        AssetBundleInfo bundle = null;
        m_LoadedAssetBundles.TryGetValue(abName, out bundle);
        if (bundle == null) return null;

        // No dependencies are recorded, only the bundle itself is required.
        string[] dependencies = null;
        if (!m_Dependencies.TryGetValue(abName, out dependencies))
            return bundle;

        // Make sure all dependencies are loaded
        foreach (var dependency in dependencies)
        {
            AssetBundleInfo dependentBundle;
            m_LoadedAssetBundles.TryGetValue(dependency, out dependentBundle);
            if (dependentBundle == null) return null;
        }
        return bundle;
    }

    /// <summary>
    /// 获得真实的ab包，即存在在StreamingAssets中的
    /// </summary>
    /// <param name="abName"></param>
    /// <returns></returns>
    private string GetRealAssetPath(string abName)
    {
        if (abName.Equals(AppConst.AssetDir))
        {
            return abName;
        }
        abName = abName.ToLower();
        if (!abName.EndsWith(AppConst.ExtName))
        {
            abName += AppConst.ExtName;
        }
        if (abName.Contains("/"))
        {
            return abName;
        }

        for (int i = 0; i < m_AllManifest.Length; i++)
        {
            int index = m_AllManifest[i].LastIndexOf('/');
            string path = m_AllManifest[i].Remove(0, index + 1);    //字符串操作函数都会产生GC
            if (path.Equals(abName))
            {
                return m_AllManifest[i];
            }
        }
        Debug.LogError("GetRealAssetPath Error:>>" + abName);
        return null;
    }

    private void UnloadDependencies(string abName, bool isThorough)
    {
        string[] dependencies = null;
        if (!m_Dependencies.TryGetValue(abName, out dependencies))
            return;

        // Loop dependencies.
        foreach (var dependency in dependencies)
        {
            UnloadAssetBundleInternal(dependency, isThorough);
        }
        m_Dependencies.Remove(abName);
    }

    private void UnloadAssetBundleInternal(string abName, bool isThorough)
    {
        AssetBundleInfo bundle = GetLoadedAssetBundle(abName);
        if (bundle == null) return;

        if (--bundle.m_ReferencedCount <= 0)
        {
            // 如果当前AB处于Async Loading过程中，卸载会崩溃，只减去引用计数即可
            if (m_LoadRequests.ContainsKey(abName))
            {
                return;     
            }
            bundle.m_AssetBundle.Unload(isThorough);
            m_LoadedAssetBundles.Remove(abName);
            Debug.Log(abName + " has been unloaded successfully");
        }
    }
}
