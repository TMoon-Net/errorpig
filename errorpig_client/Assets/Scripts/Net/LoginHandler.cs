﻿using Protocol;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginHandler : MonoBehaviour, IHandler
{
    public void MessageReceive(SocketModel model)
    {
        switch (model.Command)
        {
            case LoginProtocol.LOGIN_SRES:
                Login(model.GetMessage<int>());
                break;
            case LoginProtocol.REG_SRES:
                Register(model.GetMessage<int>());
                break;
        }
    }

    public void Login(int value)
    {
        switch (value)
        {
            case 0:
                Debug.Log("登陆成功！");
                break;
            case -1:
                Debug.Log("账号不存在！");
                break;
            case -2:
                Debug.Log("账号在线！");
                break;
            case -3:
                Debug.Log("密码错误！");
                break;
            case -4:
                Debug.Log("输入不合法！");
                break;
        }
    }

    public void Register(int value)
    {
        switch (value)
        {
            case 0:
                Debug.Log("注册成功！");
                break;
            case 1:
                Debug.Log("注册失败，帐号重复！");
                break;
            default:
                break;
        }
    }
}
