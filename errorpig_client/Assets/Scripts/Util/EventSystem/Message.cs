﻿using System;

public class Message : IMessage
{
    protected object mData = null;
    public object Data
    {
        get { return mData; }
        set { mData = value; }
    }

    protected float mDelay = 0;
    public float Delay
    {
        get { return mDelay; }
        set { mDelay = value; }
    }

    protected bool mIsSent = false;
    public bool IsSent
    {
        get { return mIsSent; }
        set { mIsSent = value; }
    }

    protected string mType = String.Empty;
    public string Type
    {
        get { return mType; }
        set { mType = value; }
    }

    protected string mFilter = String.Empty;
    public string Filter
    {
        get { return mFilter; }
        set { mFilter = value; }
    }

    public void Reset()
    {
        mType = String.Empty;
        mData = null;
        mIsSent = false;
        mFilter = string.Empty;
        mDelay = 0.0f;
    }

}

