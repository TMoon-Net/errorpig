﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Protocol;
using Protocol.DTO;

public class ServerLoginTest : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            AccountInfoDTO dto = new AccountInfoDTO();
            dto.Account = "123";
            dto.Password = "123";
            NetManager.Instance.Write(GameProtocol.TYPE_LOGIN,0,LoginProtocol.LOGIN_CREQ,dto);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            AccountInfoDTO dto = new AccountInfoDTO();
            dto.Account = "123";
            dto.Password = "123";
            NetManager.Instance.Write(GameProtocol.TYPE_LOGIN, 0, LoginProtocol.REG_CREQ, dto);
        }
    }
}
