﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceUpdate_Script : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        ResourceManager.Instance.LoadPrefab("noticepanel", "NoticePanel",(obj) => {
            GameObject.Instantiate(obj[0],GameObject.Find("Canvas").transform);
        });

    }
}
