﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class HotUpdateDemo_Script : MonoBehaviour
{
    public Text InfomationText;
    public Image ProgressBar;
    public Transform Dancer;
    public GameObject UpdatePanel;
    public Text UpdateInfomation;
    public Text DebugText;
    public GameObject[] HideGameObjects;
    public GameObject[] ShowGameObjects;

    private bool updated = false;

    private void Awake()
    {
        // 方便在手机看调试信息
        Application.logMessageReceived += ((condition, stackTrace, type) =>
        {
            DebugText.text += string.Format("调试类型为:{2}\n 详情:{0}\n代码追踪:{1}\r\n", condition, stackTrace, type);
        });
    }

    private void Start()
    {
        // 用来测试解压和下载的卡帧情况
        Dancer.DOMove(new Vector3(200, 0, 0), 1).SetRelative().SetLoops(-1, LoopType.Yoyo);

        UpdateManager.Instance.OnFinishUpdateEvent += () =>
        {
            ResourceManager.Instance.Initialize(AppConst.AssetDir, () =>
            {
                Debug.Log("ResourceManager.Instance.Initialize Finished!!!");

                LuaManager.Instance.InitStart();

                foreach (var item in HideGameObjects)
                {
                    item.SetActive(false);
                }

                foreach (var item in ShowGameObjects)
                {
                    item.SetActive(true);
                }
            });
        };
    }

    private void Update()
    {
        if (updated)
        {
            return;
        }

        switch (UpdateManager.Instance.UpdateState)
        {
            case UpdateState.CheckExtract:
                InfomationText.text = "正在检查资源...";
                ProgressBar.fillAmount = 0;
                break;
            case UpdateState.Extracting:
                float extractedProgress = (float)UpdateManager.Instance.ExtractedAmount / UpdateManager.Instance.ExtractedTotal;
                InfomationText.text = "正在解压资源...";
                ProgressBar.fillAmount = extractedProgress;
                break;
            case UpdateState.FinishExtract:
                InfomationText.text = "资源解压完毕...";
                ProgressBar.fillAmount = 1;
                break;
            case UpdateState.CheckUpdate:
                InfomationText.text = "正在检查更新...";
                ProgressBar.fillAmount = 0;
                break;
            case UpdateState.SelectUpdate:
                if (UpdateManager.Instance.IsUpdate)
                {
                    UpdatePanel.SetActive(false);
                }
                else
                {
                    UpdatePanel.SetActive(true);
                    UpdateInfomation.text = "更新大小:" + (UpdateManager.Instance.UpdateTotal / 1024 / 1024).ToString("0.00") + "MB";
                }
                break;
            case UpdateState.Updating:
                float updateProgress = (float)UpdateManager.Instance.UpdateAmount / UpdateManager.Instance.UpdateTotal;
                InfomationText.text = "正在更新资源...";
                ProgressBar.fillAmount = updateProgress;
                break;
            case UpdateState.FinishUpdate:
                InfomationText.text = "资源更新完毕...";
                ProgressBar.fillAmount = 1;
                updated = true;
                break;
        }
    }

    public void OnUpdataBtnEvent()
    {
        UpdateManager.Instance.IsUpdate = true;
    }

    public void OnExitBtnEvent()
    {
        Application.Quit();
    }
}
