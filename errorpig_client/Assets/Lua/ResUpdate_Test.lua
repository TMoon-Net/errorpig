GameObject = UnityEngine.GameObject

local this = {}

function this:Awake()
    ResourceManager.Instance:LoadPrefab("noticepanel",{"NoticePanel"},function(objs) 
        GameObject.Instantiate(objs[0],GameObject.Find("Canvas").transform);
    end)
end

return this