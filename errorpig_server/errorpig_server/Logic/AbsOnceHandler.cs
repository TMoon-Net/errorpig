﻿using NetFrame;
using NetFrame.Auto;
using System;
using System.Collections.Generic;
using System.Text;

namespace errorpig_server.Logic
{
    public class AbsOnceHandler
    {
        private byte type;
        private int area;

        public void SetArea(int area)
        {
            this.area = area;
        }

        public virtual int GetArea()
        {
            return area;
        }

        public void SetType(byte type)
        {
            this.type = type;
        }

        public new virtual byte GetType()
        {
            return type;
        }

        #region 通过连接对象发送
        public void Write(UserToken token, int command)
        {
            Write(token, command, null);
        }
        public void Write(UserToken token, int command, object message)
        {
            Write(token, GetArea(), command, message);
        }
        public void Write(UserToken token, int area, int command, object message)
        {
            Write(token, GetType(), GetArea(), command, message);
        }
        public void Write(UserToken token, byte type, int area, int command, object message)
        {
            byte[] value = MessageEncoding.Encode(CreateSocketModel(type, area, command, message));
            value = LengthEncoding.Encode(value);
            token.Write(value);
        }
        #endregion

        public SocketModel CreateSocketModel(byte type, int area, int command, object message)
        {
            return new SocketModel(type, area, command, message);
        }

    }
}
