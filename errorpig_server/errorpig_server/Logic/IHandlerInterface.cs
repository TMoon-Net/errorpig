﻿using NetFrame;
using NetFrame.Auto;
using System;
using System.Collections.Generic;
using System.Text;

namespace errorpig_server.Logic
{
    public interface IHandlerInterface
    {
        void ClientClose(UserToken token, string error);

        void MessageReceive(UserToken token, SocketModel message);
    }
}
