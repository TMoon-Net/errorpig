﻿using System;
using System.Collections.Generic;
using System.Text;
using NetFrame;
using NetFrame.Auto;
using errorpig_server.Biz;
using errorpig_server.Tool;
using Protocol.DTO;
using Protocol;

namespace errorpig_server.Logic.Login
{
    public class LoginHandler : AbsOnceHandler, IHandlerInterface
    {
        private IAccountBiz accountBiz = BizFactory.AccountBiz;

        public void ClientClose(UserToken token, string error)
        {
            ExecutorPool.Instance.Execute(() => accountBiz.Close(token));
        }

        public void MessageReceive(UserToken token, SocketModel message)
        {
            switch (message.Command)
            {
                case LoginProtocol.LOGIN_CREQ:
                    Login(token, message.GetMessage<AccountInfoDTO>());
                    break;
                case LoginProtocol.REG_CREQ:
                    Register(token, message.GetMessage<AccountInfoDTO>());
                    break;
            }
        }

        public void Login(UserToken token, AccountInfoDTO value)
        {
            ExecutorPool.Instance.Execute(() => {
                int result = accountBiz.Login(token, value.Account, value.Password);
                Write(token, LoginProtocol.LOGIN_SRES, result);
            });
        }

        public void Register(UserToken token, AccountInfoDTO value)
        {
            ExecutorPool.Instance.Execute(() => {
                int result = accountBiz.Create(token, value.Account, value.Password);
                Write(token, LoginProtocol.REG_SRES, result);
            });
        }

        public override byte GetType()
        {
            return GameProtocol.TYPE_LOGIN;
        }
    }
}
