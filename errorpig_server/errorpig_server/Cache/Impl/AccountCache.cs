﻿using System;
using System.Collections.Generic;
using System.Text;
using NetFrame;
using errorpig_server.Model;

namespace errorpig_server.Cache.Impl
{
    public class AccountCache : IAccountCache
    {
        private int index = 0;
        /// <summary>
        /// 玩家连接对象与账号的映射绑定
        /// </summary>
        private Dictionary<UserToken, string> onlineAccMap = new Dictionary<UserToken, string>();
        /// <summary>
        /// 账号与自身具体属性的映射绑定
        /// </summary>
        private Dictionary<string, AccountModel> accMap = new Dictionary<string, AccountModel>();

        public bool HasAccount(string account)
        {
            return accMap.ContainsKey(account);
        }

        public bool Match(string account, string password)
        {
            //判断账号是否存在 不存在就谈不上匹配了
            if (!HasAccount(account))
            {
                return false;
            }
            //获取账号的信息 判断密码是否匹配并返回
            return accMap[account].Password.Equals(password);
        }

        public bool IsOnline(string account)
        {
            //判断当前在线字典中 是否有此账号  没有则说明不在线
            return onlineAccMap.ContainsValue(account);
        }

        public int GetId(NetFrame.UserToken token)
        {
            //判断在线字典中是否有此连接的记录  没有说明此连接没有登陆 无法获取到账号id
            if (!onlineAccMap.ContainsKey(token))
            {
                return -1;
            }
            //返回绑定账号的id
            return accMap[onlineAccMap[token]].Id;
        }

        public void Online(NetFrame.UserToken token, string account)
        {
            //添加映射
            onlineAccMap.Add(token, account);
        }

        public void Offline(NetFrame.UserToken token)
        {
            //如果当前连接有登陆 进行移除
            if (onlineAccMap.ContainsKey(token))
            {
                onlineAccMap.Remove(token);
            }
        }

        public void Add(string account, string password)
        {
            //创建账号实体并进行绑定
            AccountModel model = new AccountModel();
            model.Account = account;
            model.Password = password;
            model.Id = index;
            accMap.Add(account, model);
            index++;
        }
    }
}
