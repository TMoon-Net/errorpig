﻿using System;
using System.Collections.Generic;
using System.Text;

namespace errorpig_server.Model
{
    public class AccountModel
    {
        public int Id { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
    }
}
