﻿using NetFrame;
using NetFrame.Auto;
using System;
using System.Collections.Generic;
using System.Text;

namespace errorpig_server
{
    class Program
    {
        static void Main(string[] args)
        {

            //服务器初始化
            ServerStart ss = new ServerStart(9000);
            ss.Encode = MessageEncoding.Encode;
            ss.Decode = MessageEncoding.Decode;
            ss.HandlerCenter = new HandlerCenter();
            ss.LengthDecode = LengthEncoding.Decode;
            ss.LengthEncode = LengthEncoding.Encode;
            ss.Start(6650);
            Console.WriteLine("服务器启动成功");
            while (true) { }
        }
    }
}
