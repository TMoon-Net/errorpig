﻿using errorpig_server.Logic;
using errorpig_server.Logic.Login;
using NetFrame;
using NetFrame.Auto;
using Protocol;
using System;
using System.Collections.Generic;
using System.Text;

namespace errorpig_server
{
    public class HandlerCenter : AbsHandlerCenter
    {
        IHandlerInterface loginHandler;

        public HandlerCenter()
        {
            loginHandler = new LoginHandler();
        }

        public override void ClientClose(UserToken token, string error)
        {
            Console.WriteLine("有客户端断开连接了");
            loginHandler.ClientClose(token, error);
        }

        public override void ClientConnect(UserToken token)
        {
            Console.WriteLine("有客户端连接上了");
        }

        public override void MessageReceive(UserToken token, object message)
        {
            SocketModel model = message as SocketModel;

            switch (model.Type)
            {
                case GameProtocol.TYPE_LOGIN:
                    loginHandler.MessageReceive(token, model);
                    break;
                default:
                    // 未知模块  可能是客户端作弊了 无视
                    break;
            }

            Console.WriteLine("收到消息为：" + message.ToString());
        }
    }
}
