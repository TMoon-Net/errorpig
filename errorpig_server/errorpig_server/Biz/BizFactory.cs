﻿using errorpig_server.Biz.Impl;
using System;
using System.Collections.Generic;
using System.Text;

namespace errorpig_server.Biz
{
    public class BizFactory
    {
        public readonly static IAccountBiz AccountBiz;

        static BizFactory()
        {
            AccountBiz = new AccountBiz();
        }

    }
}
