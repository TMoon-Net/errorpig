﻿using errorpig_server.Cache;
using NetFrame;
using System;
using System.Collections.Generic;
using System.Text;

namespace errorpig_server.Biz.Impl
{
    public class AccountBiz : IAccountBiz
    {
        IAccountCache accountCache = CacheFactory.AccountCache;

        public int Create(UserToken token, string account, string password)
        {
            if (accountCache.HasAccount(account))
            {
                return 1;
            }
            accountCache.Add(account, password);
            return 0;
        }

        public int Login(UserToken token, string account, string password)
        {
            //账号密码为空 输入不合法
            if (account == null || password == null)
            {
                return -4;
            }
            //判断账号是否存在  不存在则无法登陆
            if (!accountCache.HasAccount(account))
            {
                return -1;
            }
            //判断此账号当前是否在线
            if (accountCache.IsOnline(account))
            {
                return -2;
            }
            //判断账号密码是否匹配
            if (!accountCache.Match(account, password))
            {
                return -3;
            }
            //验证都通过 说明可以登录  调用上线并返回成功
            accountCache.Online(token, account);
            return 0;
        }

        public void Close(UserToken token)
        {
            accountCache.Offline(token);
        }

        public int Get(UserToken token)
        {
            return accountCache.GetId(token);
        }
    }
}
