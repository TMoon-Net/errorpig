﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Protocol.DTO
{
    [Serializable]
    public class AccountInfoDTO
    {
        public string Account { get; set; }
        public string Password { get; set; }
    }
}
